```
███████╗████████╗██████╗ ██╗   ██╗ ██████╗████████╗██╗   ██╗██████╗ ███████╗
██╔════╝╚══██╔══╝██╔══██╗██║   ██║██╔════╝╚══██╔══╝██║   ██║██╔══██╗██╔════╝
███████╗   ██║   ██████╔╝██║   ██║██║        ██║   ██║   ██║██████╔╝█████╗  
╚════██║   ██║   ██╔══██╗██║   ██║██║        ██║   ██║   ██║██╔══██╗██╔══╝  
███████║   ██║   ██║  ██║╚██████╔╝╚██████╗   ██║   ╚██████╔╝██║  ██║███████╗
╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝  ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝
                                                                                                                                                                                      
 __  This is the         __                        
/__ _ __  _  __ _  |    (_ _|_ __    _ _|_    __ _ 
\_|(/_| |(/_ | (_| |    __) |_ | |_|(_  |_|_| | (/_
                    of the game, and how I shall implement it.   
```

## Actions
- Pass
- Explore (1 disc)
  - Randomly generate hex I, II, or III and coordinates accordingly
    + Limited number of I and II hexes
  - Choose to colonise (1 disc)
    + Populate planets (1 or more cubes, increase production rates)
      + If advanced, do checks
- Influence (1 disc)
  - Remove disc and colonisation from hex (-1 disc)
- Research (1 disc)
  - Choose certain number of researchable<sup>[1](#footnote-1)</sup>
  - Pay with science
- Upgrade (1 disc)
  - Modify array of Modules (on ship)
  - Choose 2 of researched thingies to upgrade ships with
- Build (1 disc)
  - Use material to buy certain number of ships
  - Place on specified Hex
- Move (1 disc)
  - Move chosen ships by predetermined amounts
  - 1x that amount for two ships or 2x that amount for 1 ship

## Player Object
- Variables
  - Races
    - Each race's effects stored in YAML/JSON
  - Current values and production rates<sup>[2](#footnote-2)</sup> for:
    + Material
    + Science
    + Money
  - Number of builds/researches allowed per turn (can vary based on race)
  - Icon/Face if I'm feeling up to it
  - Array of Ship objects.
  - Modules for each ship type
    + Might have to be hardcoded array of Module/Upgrade objects for each ship type
  - Array of researched options
  - Array of hexes owned? (mightn't be neccessary if game stores hex vals, and those are passsed into Player)
  - Passed or not
  - Cost of round (this will increment as the round continues and you make more turns)<sup>[3](#footnote-3)</sup>
  - Reduction in research cost for each section (and victory points associated with it) based on how many have been reasearched in that section
  - Functions
    - Turn (this could be handled by [game](#game-object) object...)
      + If not passed
        - Gets user input based on actions (this action could be a pass)
        - Modifies relevant variables
        - Updates round cost
      + else
        - continue

## Hex Object
- Oh these could look really cooooool in processing
- Variables
  - Coords on grid
  - Array of Planet objects
  - Owner (maybe player ID or pointer to [player object](#player-object))
  - Ancients (Array of [ship objects](#ship-object))
  - Reward (Victory points or extra [ship](#ship-object) or something)
  - Wormhole positions/rotation (oh god doing that in processing...)

## Ship Object
##### Could be generated from JSON/YAML
- Variables
  - Cost
  - [Hex](#hex-object)/coordinates that it is assigned to
  - Owner
  - [Modules](#module-object) associated with it
  - Various ship parameters effected by modules (if using Rust, use struct?)
    - Travel distance per turn
    - HP
    - Plasma Missiles/Cannons (during battles)
    - General Ion Cannons
    - Power production
    - Regeneration (? not sure if effected by modules)
- Functions
  - Move
    + Very self explanatory

## Research Object
##### Could be generated from JSON/YAML (struct?)
- Variables
  - Type (placed on ship/passive)
  - If placed on ship, Module/Upgrade associated with it
  - Max/min cost science based on player-specific reduction
  - Effects on player variables (e.g. discs)

## Module Object
##### Could be generated from JSON/YAML (struct?)
- Variables
  - Effects on [ship](#ship-object) parameters

## Game Object
##### Can be transferred to/from intermediate format such as JSON for storage in MongoDB allowing RESTful server
- Variables
  - [Players](#player-object)
    - Associated UID for use with JWTs? (JWTs could be stored with server)
  - Spectators?
  - [Hexes](#hex-object)
    - Associated coordinate system
  - [Research](#research-object) objects available for purchase
  - Number of turns completed
    - Perhaps host specified turn limit?
  - Hexes left/available in I, II spots (maybe even III if I amn't feeling nice)
  - Password for entry?
    - Host specified
  - <a name="gstateinfo"></a>Game state
    - Explore in progress
    - Waiting for turn choice (and player UID we're waiting for)
    - Battling
- Functions
  - Create
    - Given password, turn limit, max players etc.
  - Add player
    - Perhaps require password
  - Remove player
  - Each [action](#actions) as a separate function
    - Args are player UID/JWT and action-specific args
    - e.g. Explore will need to send back an intermediate request for choosing whether to discard/colonise/use planets for player to choose
    - Modify game arguments to suit

---
---

# Gameplay
This could be over a socket, maybe even allowing for auth-less games. I should probably (definitely) do auth though    

| Client (let's say player 1) | Transit | Server |    
| --------------------------- | ------- | ------ |    
| I want to join a game | -> (game id \[and password\]) | Checks if game is available / password is correct |   
| Updates UI | <- (auth<sup>[5](#footnote-5)</sup> and game state<sup>[4](#footnote-4)</sup>) | OK here you go |
|  | <- (each player's turn and game state after each turn)) | Hey this person did their turn |
|  | <- ("it's your turn" + game state) | Everyone else has done their turn, your turn |
| Gets player's turn | -> (turn information with auth) | Runs correct function on associated game object |   
| Updates UI | <- (game state) | Returns game state |    
| ... | ... | ... |    
| Last person to pass | -> (turn information with auth) | Runs battles |   
|    | <- (battle results to all clients, game state) | Sends everyone the results for UI updates (maybe even animations!!!) |
| Repeat | Until | Finished |     

### Important features of the server
- Ability to reconnect Player after game start for save games and accidental disconnects (to avoid what [this](http://treason.thebrown.net/) does `*screams in coup*`)
- JWT-based authentication
- Backed up in Mongo
- Robust and secure
- **DOCKERIZED** (as Dad would say)
- Preferably multi threaded
- Easily maintained

---
### Footnotes
<a name="footnote-1">1</a>: Researchable being research objects chosen to be added to a dictionary/array at the beginning of each round.    
<a name="footnote-2">2</a>: Production rates are `[3,4,6,8,10,12,15,18,21,24]`. Maybe these can be stored as indices to an array.  
<a name="footnote-3">3</a>: Round costs are related to footnote 1, although it is `[0,0,1,3,5,7,10,13 ...]`. Also stored as indices to make adjustable. Must remember to make these offsetted at beginning of each round by colonised planets.  
<a name="footnote-4">4</a>: This includes [these](#gstateinfo) hexes, ships, researches available, etc.    
<a name="footnote-5">5</a>: This is probably going to be a JWT, ([rust](https://github.com/Keats/jsonwebtoken), [node.js](https://www.npmjs.com/package/jsonwebtoken)). I could use oauth or something as well but it'd be nice to be self-contained.   