# Eclipse

An implementation of Eclipse: New Dawn For The Galaxy in a language of my choice. Could do a server in rust/python/nodejs (nodejs unlikely, not to sure on js classes yet) and a client in processing/p5 for easy UI and maybe some good graphics.

# Goals
- Robust, proper error-handling server
- Robust enough to have anybody make their own clients
- Easy to modify (races, ship types, etc.)
- Fast
- Multi-threaded (in Rust this may be an issue/difficult, node.js it'd be a breeze though...)